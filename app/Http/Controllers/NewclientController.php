<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Newclient;

class NewclientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = \Request::get('search');
        $newclients = Newclient::where('clientname','like','%'.$search.'%')->orderBy('id')->paginate(6);
        return view('newclient.index',['newclients' => $newclients]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('newclient.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $this->validate($request,[
            'clientname'=>'required',
            'reference'=>'required',
            'organization'=>'required',
            'item'=>'required',
            'contact'=>'required',
            'date'=>'required',
            'time'=>'required',
        ]);
        //create new data
        $newclient = new newclient;
        $newclient->clientname=$request->clientname;
        $newclient->reference=$request->reference;
        $newclient->organization=$request->organization;
        $newclient->item=$request->item;
        $newclient->contact=$request->contact;
        $newclient->date=$request->date;
        $newclient->time=$request->time;
        $newclient->save();
        return redirect()->route('newclient.index')->with('alert-success','Data Has Been Save!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('newclient.show',compact('newclient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $newclient = Newclient::findOrFail($id);
        return view('newclient.edit',compact('newclient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $this->validate($request,[
            'clientname'=>'required',
            'reference'=>'required',
            'organization'=>'required',
            'item'=>'required',
            'contact'=>'required',
            'date'=>'required',
            'time'=>'required',
        ]);
        //create new data
        $newclient = new newclient;
        $newclient->clientname=$request->clientname;
        $newclient->reference=$request->reference;
        $newclient->organization=$request->organization;
        $newclient->item=$request->item;
        $newclient->contact=$request->contact;
        $newclient->date=$request->date;
        $newclient->time=$request->time;
        $newclient->save();
        return redirect()->route('newclient.index')->with('alert-success','Data Hasbeen Saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //DELETE DATA
        $newclient = Newclient::findOrFail($id);
        $newclient->delete();
        return redirect()->route('newclient.index')->with('alert-success','Data Has Been Deleted!');

    }
}
