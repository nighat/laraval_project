<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Client;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *client
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //show Data
        //$clients = Client::all();
        //$clients =DB::table('client')->paginate(5);
        // $clients = Client::paginate(5);
        //return view('client.index',['clients'=>$clients]);
        $search = \Request::get('search');
        $clients = Client::where('clientname','like','%'.$search.'%')->orderBy('id')->paginate(6);
        return view('client.index',['clients' => $clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $this->validate($request,[
            'clientname'=>'required',
            'reference'=>'required',
            'organization'=>'required',
            'item'=>'required',
            'order_date'=>'required',
            'renew_date'=>'required',
            'advance'=>'required',
            'dues'=>'required',
            'total'=>'required',
            'email'=>'required',
            'contact'=>'required',
        ]);
        //create new data
        $client = new client;
        $client->clientname=$request->clientname;
        $client->reference=$request->reference;
        $client->organization=$request->organization;
        $client->item=$request->item;
        $client->order_date=$request->order_date;
        $client->renew_date=$request->renew_date;
        $client->advance=$request->advance;
        $client->dues=$request->dues;
        $client->total=$request->total;
        $client->email=$request->email;
        $client->contact=$request->contact;
        $client->save();
        return redirect()->route('client.index')->with('alert-success','Data Has Been Save!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);
        return view('client.edit',compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validation
        $this->validate($request,[
            'clientname'=>'required',
            'reference'=>'required',
            'organization'=>'required',
            'item'=>'required',
            'order_date'=>'required',
            'renew_date'=>'required',
            'advance'=>'required',
            'dues'=>'required',
            'total'=>'required',
            'email'=>'required',
            'contact'=>'required',
        ]);

        $client = Client::findOrFail($id);
        $client->clientname = $request->clientname;
        $client->reference = $request->reference;
        $client->organization = $request->organization;
        $client->item = $request->item;
        $client->order_date = $request->order_date;
        $client->renew_date = $request->renew_date;
        $client->advance = $request->advance;
        $client->dues = $request->dues;
        $client->total = $request->total;
        $client->email = $request->email;
        $client->contact = $request->contact;
        $client->save();

        return redirect()->route('client.index')->with('alert-success','Data Hasbeen Saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //DELETE DATA
        $client = Client::findOrFail($id);
        $client->delete();
        return redirect()->route('client.index')->with('alert-success','Data Has Been Deleted!');

    }
}
