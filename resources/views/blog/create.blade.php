@extends('master')
@section('content')
    <div class="row">

        <div class="col-md-1-12">
            <h1><canter> New Blog Form</canter></h1>

        </div>
    </div>
    <div class="row">

        <div class="col-md-12" >

            <form class="" action="{{route('blog.store')}}" method="post">
                {{csrf_field()}}
                <table>
                    <th>Name</th>
                <tr><td><div class="form-group{{($errors->Has('title'))?$errors->first('title'):''}}">
                <input type="text" name="title" class="form-control" placeholder="Enter Title here">
                    {!! $errors->first('title','<p class="help-block">:message</p>') !!}
                </div></td></tr>
                    <th>Descripation</th>
                    <tr><td>
                <div class="form-group{{($errors->Has('description'))?$errors->first('description'):''}}">
                    <input type="text" name="description" class="form-control" placeholder="Enter description here">
                    {!! $errors->first('description','<p class="help-block">:message</p>') !!}
                </div>
                            </td></tr>
                    <tr><td>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="save">
                </div>
                    </td></tr>
                    </table>
            </form>
        </div>

    </div>

    @stop