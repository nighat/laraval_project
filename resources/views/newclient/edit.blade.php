@extends('master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 style="text-align: center"> Edit Appointment Data</h3>
        </div>
    </div>
    <div class="row">
        <form class="" action="{{route('newclient.update',$newclient->id)}}" method="post">
            <input name="_method" type="hidden" value="PATCH">
            {{csrf_field()}}
            <table  >

                <tr style="text-align: center"><td style="padding-left:350px " >
                    <td> <p>Client Name:</p>
                        <div class="form-group{{($errors->Has('clientname'))?$errors->first('clientname'):''}}">
                            <input type="text" name="clientname" class="form-control" placeholder="Enter clientname here" value="{{$newclient->clientname}}">
                            {!! $errors->first('clientname','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px "> <p>Reference:</p>
                        <div class="form-group{{($errors->Has('reference'))?$errors->first('reference'):''}}">
                            <input type="text" name="reference" class="form-control" placeholder="Enter reference here" value="{{$newclient->reference}}">
                            {!! $errors->first('reference','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>

                <tr style="text-align: center"><td style="padding-left:300px " >
                    <td> <p>Organization</p>
                        <div class="form-group{{($errors->Has('organization'))?$errors->first('organization'):''}}">
                            <input type="text" name="organization" class="form-control" placeholder="Enter organization here" value="{{$newclient->organization}}">
                            {!! $errors->first('organization','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px "> <p>Item</p>
                        <div class="form-group{{($errors->Has('item'))?$errors->first('item'):''}}">
                            <input type="text" name="item" class="form-control" placeholder="Enter item here" value="{{$newclient->item}}">
                            {!! $errors->first('item','<p class="help-block">:message</p>') !!}
                        </div>

                    </td>
                </tr>


                <tr style="text-align: center"><td style="padding-left:300px " >


                    <td> <p>Contact</p>
                        <div class="form-group{{($errors->Has('contact'))?$errors->first('contact'):''}}">
                            <input type="number" name="contact" class="form-control" placeholder="Enter email here" value="{{$newclient->contact}}" >
                            {!! $errors->first('contact','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>

                    <td> <p>Contact Date</p>
                        <div class="form-group{{($errors->Has('date'))?$errors->first('date'):''}}">
                            <input type="date" name="date" class="form-control" placeholder="Enter date here" value="{{$newclient->date}}" >
                            {!! $errors->first('date','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>

                <tr style="text-align: center"><td style="padding-left:300px " >

                    <td> <p>Contact Time</p>
                        <div class="form-group{{($errors->Has('time'))?$errors->first('time'):''}}">
                            <input type="datetime" name="time" class="form-control" placeholder="Enter time here" value="{{$newclient->time}}" >
                            {!! $errors->first('time','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px ">
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="save">
                        </div>
                    </td>
                </tr>

            </table>
        </form>
    </div>
@stop