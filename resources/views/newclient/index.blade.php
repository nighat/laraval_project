@extends('master')
@section('content')
    <div class="form-group row add">
        <div class="col-md-6">
            <h2 style="text-align: center">Appointment Information</h2>
        </div>
    </div>
    <form class="navbar-form navbar-right" role="search">
        <div class="form-group">
            <input type="text" name="search" class="form-control" placeholder="search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form
    <div class="row">
        <table class="table table-striped">
            <tr>
                <th> No.</th>
                <th> client Name</th>
                <th> Reference</th>
                <th> Organization</th>
                <th> Item</th>
                <th> Contact</th>
                <th> Contact Date</th>
                <th> Contact Time</th>
                <th> Action</th>
            </tr>
            <a href="{{(route('newclient.create'))}}" class="btn btn-info pull-left">New Data</a><br>
            <?php $no=1;?>
            @foreach($newclients as $newclient)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$newclient->clientname}}</td>
                    <td>{{$newclient->reference}}</td>
                    <td>{{$newclient->organization}}</td>
                    <td>{{$newclient->item}}</td>
                    <td>{{$newclient->contact}}</td>
                    <td>{{$newclient->date}}</td>
                    <td>{{$newclient->time}}</td>
                    <td>
                        <form class="" action="{{route('newclient.destroy',$newclient->id)}}" method="post">
                            <input type="hidden" name="_method" value="delete">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <a href="{{route('newclient.edit',$newclient->id)}}" class="btn btn-primary">Edit</a>
                            <input type="submit" class="btn btn-danger" onclick="return confirm('Are you sure to delete this data');" name="name" value="delete">



                        </form>
                    </td>
                </tr>
            @endforeach

        </table>
    </div>
      {!! $newclients->links() !!}
@stop

