@extends('master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 style="text-align: center">Edit Data</1h>
        </div>
    </div>
    <div class="row">
        <form class="" action="{{route('client.update',$client->id)}}" method="post">
            <input name="_method" type="hidden" value="PATCH">
            {{csrf_field()}}
            <table  >

                <tr style="text-align: center"><td style="padding-left:350px " >
                    <td> <p>Client Name:</p>
                        <div class="form-group{{($errors->Has('clientname'))?$errors->first('clientname'):''}}">
                            <input type="text" name="clientname" class="form-control" placeholder="Enter clientname here" value="{{$client->clientname}}">
                            {!! $errors->first('clientname','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px "> <p>Reference:</p>
                        <div class="form-group{{($errors->Has('reference'))?$errors->first('reference'):''}}">
                            <input type="text" name="reference" class="form-control" placeholder="Enter reference here" value="{{$client->reference}}">
                            {!! $errors->first('reference','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>

                <tr style="text-align: center"><td style="padding-left:300px " >
                    <td> <p>Organization</p>
                        <div class="form-group{{($errors->Has('organization'))?$errors->first('organization'):''}}">
                            <input type="text" name="organization" class="form-control" placeholder="Enter organization here" value="{{$client->organization}}">
                            {!! $errors->first('organization','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px "> <p>Item</p>
                        <div class="form-group{{($errors->Has('item'))?$errors->first('item'):''}}">
                            <input type="text" name="item" class="form-control" placeholder="Enter item here" value="{{$client->item}}">
                            {!! $errors->first('item','<p class="help-block">:message</p>') !!}
                        </div>

                    </td>
                </tr>

                <tr style="text-align: center"><td style="padding-left:300px " >
                    <td>   <p>Order Date</p>
                        <div class="form-group{{($errors->Has('order_date'))?$errors->first('order_date'):''}}">
                            <input type="date" name="order_date" class="form-control" placeholder="Enter order date here" value="{{$client->order_date}}">
                            {!! $errors->first('order_date','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>

                    <td style="padding-left:30px "> <p>Renew Date</p>
                        <div class="form-group{{($errors->Has('renew_date'))?$errors->first('renew_date'):''}}">
                            <input type="date" name="renew_date" class="form-control" placeholder="Enter renew date here" value="{{$client->renew_date}}">
                            {!! $errors->first('renew_date','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>


                <tr style="text-align: center"><td style="padding-left:300px " >

                    <td> <p>Advance</p>
                        <div class="form-group{{($errors->Has('advance'))?$errors->first('advance'):''}}">
                            <input type="number" name="advance" class="form-control" placeholder="Enter advance here" value="{{$client->advance}}" >
                            {!! $errors->first('advance','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>


                    <td style="padding-left:30px "> <p>Dues</p>
                        <div class="form-group{{($errors->Has('dues'))?$errors->first('dues'):''}}">
                            <input type="number" name="dues" class="form-control" placeholder="Enter dues here" value="{{$client->dues}}">
                            {!! $errors->first('dues','<p class="help-block">:message</p>') !!}
                        </div>

                    </td>
                </tr>


                <tr style="text-align: center"><td style="padding-left:300px " >

                    <td> <p>Total</p>
                        <div class="form-group{{($errors->Has('total'))?$errors->first('total'):''}}">
                            <input type="number" name="total" class="form-control" placeholder="Enter total here" value="{{$client->total}}">
                            {!! $errors->first('total','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>

                    <td style="padding-left:30px "> <p>Email</p>
                        <div class="form-group{{($errors->Has('email'))?$errors->first('email'):''}}">
                            <input type="text" name="email" class="form-control" placeholder="Enter email here" value="{{$client->email}}">
                            {!! $errors->first('email','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                </tr>

                <tr style="text-align: center"><td style="padding-left:300px " >

                    <td> <p>Contact</p>
                        <div class="form-group{{($errors->Has('contact'))?$errors->first('contact'):''}}">
                            <input type="number" name="contact" class="form-control" placeholder="Enter email here" value="{{$client->contact}}" >
                            {!! $errors->first('contact','<p class="help-block">:message</p>') !!}
                        </div>
                    </td>
                    <td style="padding-left:30px ">
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="save">
                        </div>
                    </td>
                </tr>

            </table>
        </form>
    </div>
@stop