@extends('master')
@section('content')
    <div class="row">

        <div class="col-md-1-12">
            <h3 style="text-align: center"> Create Form</h3>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form class="" action="{{route('client.store')}}" method="post">
                {{csrf_field()}}
                  <table  >

                      <tr style="text-align: center"><td style="padding-left:350px " >
                        <td> <p>Client Name:</p>
                          <div class="form-group{{($errors->Has('clientname'))?$errors->first('clientname'):''}}">
                              <input type="text" name="clientname" class="form-control" placeholder="Enter clientname here">
                              {!! $errors->first('clientname','<p class="help-block">:message</p>') !!}
                          </div>
                        </td>
                        <td style="padding-left:30px "> <p>Reference:</p>
                          <div class="form-group{{($errors->Has('reference'))?$errors->first('reference'):''}}">
                              <input type="text" name="reference" class="form-control" placeholder="Enter reference here">
                              {!! $errors->first('reference','<p class="help-block">:message</p>') !!}
                          </div>
                        </td>
                      </tr>

                      <tr style="text-align: center"><td style="padding-left:300px " >
                        <td> <p>Organization</p>
                          <div class="form-group{{($errors->Has('organization'))?$errors->first('organization'):''}}">
                             <input type="text" name="organization" class="form-control" placeholder="Enter organization here">
                             {!! $errors->first('organization','<p class="help-block">:message</p>') !!}
                          </div>
                        </td>
                        <td style="padding-left:30px "> <p>Item</p>
                          <div class="form-group{{($errors->Has('item'))?$errors->first('item'):''}}">
                               <input type="text" name="item" class="form-control" placeholder="Enter item here">
                                {!! $errors->first('item','<p class="help-block">:message</p>') !!}
                          </div>

                        </td>
                      </tr>

                      <tr style="text-align: center"><td style="padding-left:300px " >
                         <td>   <p>Order Date</p>
                            <div class="form-group{{($errors->Has('order_date'))?$errors->first('order_date'):''}}">
                              <input type="date" name="order_date" class="form-control" placeholder="Enter order date here">
                              {!! $errors->first('order_date','<p class="help-block">:message</p>') !!}
                            </div>
                         </td>

                          <td style="padding-left:30px "> <p>Renew Date</p>
                            <div class="form-group{{($errors->Has('renew_date'))?$errors->first('renew_date'):''}}">
                               <input type="date" name="renew_date" class="form-control" placeholder="Enter renew date here">
                                {!! $errors->first('renew_date','<p class="help-block">:message</p>') !!}
                            </div>
                         </td>
                      </tr>


                      <tr style="text-align: center"><td style="padding-left:300px " >

                         <td> <p>Advance</p>
                             <div class="form-group{{($errors->Has('advance'))?$errors->first('advance'):''}}">
                                <input type="number" name="advance" class="form-control" placeholder="Enter advance here">
                                 {!! $errors->first('advance','<p class="help-block">:message</p>') !!}
                             </div>
                         </td>


                          <td style="padding-left:30px "> <p>Dues</p>
                             <div class="form-group{{($errors->Has('dues'))?$errors->first('dues'):''}}">
                                 <input type="number" name="dues" class="form-control" placeholder="Enter dues here">
                                  {!! $errors->first('dues','<p class="help-block">:message</p>') !!}
                             </div>

                         </td>
                      </tr>


                      <tr style="text-align: center"><td style="padding-left:300px " >

                         <td> <p>Total</p>
                              <div class="form-group{{($errors->Has('total'))?$errors->first('total'):''}}">
                                  <input type="number" name="total" class="form-control" placeholder="Enter total here">
                                  {!! $errors->first('total','<p class="help-block">:message</p>') !!}
                              </div>
                         </td>

                          <td style="padding-left:30px "> <p>Email</p>
                              <div class="form-group{{($errors->Has('email'))?$errors->first('email'):''}}">
                                   <input type="text" name="email" class="form-control" placeholder="Enter email here">
                                   {!! $errors->first('email','<p class="help-block">:message</p>') !!}
                              </div>
                         </td>
                      </tr>

                      <tr style="text-align: center"><td style="padding-left:300px " >

                          <td> <p>Contact</p>
                               <div class="form-group{{($errors->Has('contact'))?$errors->first('contact'):''}}">
                                   <input type="number" name="contact" class="form-control" placeholder="Enter email here">
                                   {!! $errors->first('contact','<p class="help-block">:message</p>') !!}
                               </div>
                          </td>
                          <td style="padding-left:30px ">
                              <div class="form-group">
                                  <input type="submit" class="btn btn-primary" value="save">
                              </div>
                          </td>
                      </tr>

                      </table>
            </form>
        </div>
    </div>

@stop