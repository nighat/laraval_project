@extends('master')
@section('content')
    <div class="form-group row add">
        <div class="col-md-6">
            <h2 style="padding-left: 200px;">Client Information</h2>

        </div>

    </div>
    <form class="navbar-form navbar-right" role="search">
        <div class="form-group">
            <input type="text" name="search" class="form-control" placeholder="search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <div class="row">
        <table class="table table-striped">
            <tr>
                <th> No.</th>
                <th> Client Name</th>
                <th> Reference</th>
                <th> Organization</th>
                <th> Item</th>
                <th> Order Date</th>
                <th> Renew Date</th>
                <th> Advance</th>
                <th> Dues</th>
                <th> Total</th>
                <th> Email</th>
                <th> Contact</th>
                <th> Action</th>
            </tr>
            <a href="{{(route('client.create'))}}" class="btn btn-info pull-left">Create New Data</a><br>
            <?php $no=1;?>
            @foreach($clients as $client)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$client->clientname}}</td>
                    <td>{{$client->reference}}</td>
                    <td>{{$client->organization}}</td>
                    <td>{{$client->item}}</td>
                    <td>{{$client->order_date}}</td>
                    <td>{{$client->renew_date}}</td>
                    <td>{{$client->advance}}</td>
                    <td>{{$client->dues}}</td>
                    <td>{{$client->total}}</td>
                    <td>{{$client->email}}</td>
                    <td>{{$client->contact}}</td>
                    <td>
                        <form class="" action="{{route('client.destroy',$client->id)}}" method="post">
                            <input type="hidden" name="_method" value="delete">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <a href="{{route('client.edit',$client->id)}}" class="btn btn-primary">Edit</a>
                            <input type="submit" class="btn btn-danger" onclick="return confirm('Are you sure to delete this data');" name="name" value="delete">



                        </form>
                    </td>
                </tr>
            @endforeach

        </table>
    </div>
      {!! $clients->links() !!}
@stop

